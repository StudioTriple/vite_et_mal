# Vite et mal, un workshop de prototypage typographique à la HEAD Strasbourg.

### Objectifs
Dans ce workshop nous explorerons une méthode de prototypage typographique basée sur une approche modulaire mais sans tomber dans une esthétique géométrique. Chaque étudiant développera un caractère typographique expressif inspiré par un personnage de fiction. Plus tard, des hybridations pourront naître…

### Sujet
- Vous choisirez un personnage de fiction issu de la liste ci-dessous ou de votre propre choix. Décrivez læ en 3 adjectifs. Renseignez ces informations sur [le pad du workshop](https://mensuel.framapad.org/p/viteetmal).
- Imaginez le logo / titre de film ou jeu de votre personnage de fiction. Vous réaliserez un lettrage à la main de ce mot: *bancoves* (obligatoirement en bas-de-casse) en lieu et place du nom de votre personnage. Méthode: vous dessinerez des repères sur une feuille A3 et dessinerez des lettres dont la hauteur d'œil sera d'environ 5 cm de haut.
- Vous découperez et reconnaitrez chaque module dans les parties de ces lettres
- Vous Numériserez vos modules et remplacez les miens dans ma fonte Focalisez-vous en priorité absolue sur les bas-de-casse, c'est à dire ceux surlignés en vert. Vous veillerez à éventuellement déplacer les modules dans les lettres pour que ceux-ci se combinent avec bonheur.
- Étape surprise
- Vous réaliserez un specimen en bichromie montrant votre création en vous basant sur les gabarits fournis.


Liste non exhaustive des personnages :
Bob l'éponge, Yoda, Marge Simpson, Buffy, Lara Croft, Snoopie, Elmo, Gollum, Shrek, Betty Boop, Sauron, Ghostface (Le tueur de scream), capitaine Haddock, le xénomorphe d'Alien, Ariel, Barbapapa, Zelda, Yoshi, Cruella d'Enfer, Dead Pool, Goldorak, Kirby, Kirikou, la Panthère Rose, Leia Organa (Star Wars), Pegga Pig…

### Rendus
- **jeudi 21 janvier 18:00 :** Avoir remplacé tous les modules des bas-de-casse dans ma fonte de template. Vous déposerez votre fichier de travail de fonte nommé comme suit : Nom_prénom.vfc sur [cette page](http://37.187.125.75:8000/u/d/175d6577fae74e52a469/).
- **jeudi 4 février 16:00 :** Vous produirez 2 affiches au format A2 ou homothétique, exportées aux formats .pdf et .png (1200 px de large) et déposés [ici aussi](http://37.187.125.75:8000/u/d/175d6577fae74e52a469/). Si possibilité de restitution dans l'école, vous afficherez ces specimens aux murs. Je vous ai préparé des templates de specimens, à vous de les modifier, détruire, personnaliser et faire votre.

### Planning
- **Jeudi 21/01:**
  - 10h lancement du workshop : thème, approche modulaire, choix du personnage, dessin (Type cooker)
  - 12h30-14h00 pause déjeuner
  - 14h00 présentation de la numérisation + de fontlab et de notre méthode de travail. Exercice de prise en main synchrone sur fontlab.
  - 18:00 fin de la journée
- **Vendredi 21/01:**
  - 9h30 Présentation de quelques projets de Studio Triple et de Velvetyne
  - 10h15 Reprise de l'atelier
  - 12h30-14h00 pause déjeuner
  - 15:00 Pré-rendus (et oui)
  - 15:30 Présentation sur les specimens
  - 18:00-… fin de la première partie du workshop
- (pause méritée, digestion, poursuite du travail)
- **Jeudi 04/02 (à préciser) :**
  - 9h30 lancement du dernier jour
  - 12h30-14h00 pause déjeuner
  - 16h00 Courte présentation : Pour aller plus loin.
  - 16h30 Restitution des résultats du workshop auprès de l'école

### Ressources
- [Le pad du workshop](https://mensuel.framapad.org/p/viteetmal)
- [Adhesion text](http://adhesiontext.com/) Outil qui génère du faux-texte à partir d'une liste de caractères
- [Idiotproofed](https://idiotproofed.com/)  Help you make proofs in the browser
- [Typecooker](http://typecooker.com/) Donne une recette à suivre pour un lettrage
- [Liste de ressources](/documentation/type design education ressources.md)(ENG) que j'ai mise en place et dans laquelle je vous conseille de fouiller.

### Webspecimens
- https://hypertext.futurefonts.xyz/hyperfood
- https://www.gt-super.com/
- https://www.ibm.com/plex/
- http://www.gt-alpina.com/
- https://www.gt-flexa.com/
- https://floriankarsten.github.io/space-grotesk/
- https://extraset.ch/typefaces/quarz/?mc_cid=1295f244fc&mc_eid=a8c150468d
- http://forge.cestainsi.online/modor.html
- http://compagnon.eesab.fr/index.html

### Crédits
Certains concepts et contenus de ce workshop ont été créés en collaboration avec [Océane Juvin](https://github.com/ohpla).
