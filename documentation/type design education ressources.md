# Type design ressources

## making

### Tools

- http://typecooker.com/ A tool giving you parameters for you to draw a font from 🌋
- https://adhesiontext.com/ Allows you to generate dummy text with the letters you want and only them
- https://www.urtd.net/x/typable/
- https://idiotproofed.com/  Help you make proofs in the browser
- https://workshop.mass-driver.com/waterfall Find words of the same length

### Softwares

- https://www.fontlab.com/font-editor/fontlab/7 The one you used. Cross-platform, quite complet.
- https://glyphsapp.com/ Mac only, super user friendly
   - superb documentation here https://glyphsapp.com/tutorials 🌋
   - https://sites.fastspring.com/glyphsapp/product/glyphsmini2 There is a cheaper (50$) version of Glyphs that is already super powerful 🌋
- http://robofont.com/ Scriptable powerful but dry software
- https://fontforge.org/en-US/ Free / open-source software, a bit old and harsh
- https://www.high-logic.com/font-editor/fontcreator Another software, free for non commercial typefaces


### Serious games

- https://type.method.ac/ Play with kerning
- https://shape.method.ac/ Play with Béziers

## Reading

### Books

- Counterpunch, making type in the sixteenth century, designing typefaces now, Fred Smeijers, Londres, Hyphen Press, 1996 (EN) - Les Contrepoinçons, Fred Smeijers, Paris, éditions B42, 2014 (FR) https://editions-b42.com/produit/les-contrepoincons/
- The Stroke of The Pen: fundamental aspects of western writing, Gerrit Noordzij, Koninklijke Academie van Beeldende Kunsten, The Hague (1982) (EN)
  - FR: Le Trait http://www.ypsilonediteur.com/fiche.php?id=85
- [Designing Type](https://www.placedeslibraires.fr/livre/9781786277480-designing-type-2nd-edition-karen-cheng/) by Karen Cheng: a highly detailed analysis about how to draw letter shapes in relation to each others.
- https://thamesandhudson.com/designing-fonts-an-introduction-to-professional-type-design-9780500241554 A book with a great introduction to all the basic aspects of type design and of the softwares (Glyphs, I think) 🌋
- Type Sign Symbol, Adrian Frutiger (FR/EN/DE) more about semiotics and forms perception but really interesting, visual and didactic > https://monoskop.org/images/b/b6/Frutiger_Adrian_Type_Sign_Symbol.pdf
- L'Homme et ses signes (FR), Adrian Frutiger - about history, semiotics and forms perception > http://fr.calameo.com/read/000219963e41f7488e4bf
- http://www.cnap.graphismeenfrance.fr/infini/en/download/Infini-cnap-engl_fonts-specimen-v2/03_Specimens/English_Infini-specimen.pdf Infini specimen by Sandrine Nugue with history of writing and type design, really didactic

### Blogs / where to learn online

- https://fontreviewjournal.com/ In depth analytics of fonts. Super interesting 🌋
- http://www.alphabettes.org/ A women lead blog full of resources 🌋 Also Alphabettes is offering a mentoring program, allowing mentees to get feedback on their work by mentors with more experience in the field.
- https://ohnotype.co/blog/tagged/teaching Super articles about things you want to learn in type design 🌋
- https://typedesignschool.com/ Some ($) videos tutorials about all the basic steps to type design. Seems pretty nice.
- https://type.today/en/journal A great russian blog focusing on trends, how to design specific characters such as punctuation, how to choose fonts… And a nice monthly press review.
- http://letters.temporarystate.net/
- https://www.instagram.com/attentioncaglyphe/ Instagram vulgarisation account about type
- https://ilovetypography.com/blog/ One of the most serious blogs about type design (now a foundry too)
- http://typomanie.fr/ FR pedagogic articles about type


### Articles

- https://arrowtype.github.io/type-blog/2020-05-01--getting-started-in-type/ A lot of ressources like the ones listed in this file.
- https://ohnotype.co/blog/getting-started
- https://ohnotype.co/blog/proof-it Tests and proofs
- https://ohnotype.co/blog/spacing Spacing
- https://frerejones.com/blog?tag=Education%20Mechanics
- https://www.typography.com/blog/typographic-illusions Optical corrections
- https://www.typography.com/blog/text-for-proofing-fonts What text to use for testing?

## Discovering

### Where to discover fonts

- http://fontsinuse.com/ An amazing resource showing all kind of fonts used in the wild 🌋
- https://typographica.org/ Blog + annual selection of the best fonts of the year + critics 🌋
- https://www.typewolf.com/ Same but on websites only
- https://www.getrevue.co/profile/freshfonts A monthly newsletter about new fonts
- https://typespecimens.xyz/ About online type specimen
- https://www.are.na/emmanuel-besse/type-specimens-taleuy1p7xc Old specimen
- https://letterformarchive.org/ An american archive about fonts
- http://typequality.com/ A list of wom*n type designers


### (Open source) foundries

- The league of moveable type https://www.theleagueofmoveabletype.com/
- https://typefoundry.directory/ A list of A LOT of type foundries worldwide 🌋
- Collletttivo http://collletttivo.it/
- Warsaw type https://kroje.org/en/
- Impallari Type https://github.com/impallari?tab=repositories
- Fonderie download https://www.fonderie.download/
- Love letters http://www.love-letters.be/
- OSP foundry http://osp.kitchen/foundry/
- http://www.peter-wiegel.de/
- http://www.republi.sh Revivals of vernacular Vietnamese fonts
- Velvetyne https://www.velvetyne.fr

### School foundries!

- La fonderie de l'ERG en croissance http://chasse-ouverte.erg.be
- ANRT https://anrt-nancy.fr/en/projects/
- https://gotico-antiqua.anrt-nancy.fr/en
- le 75 http://typotheque.le75.be/

### Open source type collections
- Badass Libre Fonts by Womxn https://www.design-research.be/by-womxn 🌋
- la typothèque de Luuse http://typotheque.luuse.io/
- Use and modify de Raphaël Bastide https://usemodify.com
- Fontain par Lakkon https://fontain.org/
- Codeface https://github.com/chrissimpkins/codeface
- Font Squirrel https://www.fontsquirrel.com/fonts/list/find_fonts?filter%5Blicense%5D%5B0%5D=open
- Open Foundry https://open-foundry.com
- Font Library https://fontlibrary.org
- Goofle fonts https://fonts.google.com
